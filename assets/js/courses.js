let adminUser = localStorage.getItem("isAdmin");
let cardFooter;
let modalButton  = document.querySelector("#adminButton");

if (adminUser == "false" || !adminUser) {

	modalButton.innerHTML = null;

}else {

	modalButton.innerHTML =

	`
		<div class="col-md-2 offset-md-10">
			<a href="./addCourse.html" class="btn btn-block bg-green text-white">
				Add Course
			</a>
		</div>
	`
}




fetch(`${apiUrl}/api/courses`)
.then(res => res.json())
.then(data => {

	console.log(data);

	// Creates a variable that will restore the data to be rendered
	let courseData;

	if (data.length < 1) {

		courseData = "No courses available";
		
	}else  {

		courseData = data.map(course => {

			console.log(course.isActive)

			let deleteBtn = document.querySelector('#deleteBtn')
			if(course.isActive == false || course.isActive == null) {

				deleteBtnColor = "bg-neong";
				deleteBtn = "Enable Course";

			}else {

				deleteBtnColor = "btn-danger";
				deleteBtn = "Disable Course";
			}

			if(adminUser == "false" || !adminUser) {

				cardFooter = 
					`
						<a href="./course.html?courseId=${course._id}" value="${course._id}" class="btn bg-green text-white btn-block editButton">
							Select Course
						</a>

					`

					if(course.isActive == false || !course.isActive) {
						return ('')
					}

			}else {

				cardFooter = 
					`
						<a href="./course.html?courseId=${course._id}" value="${course._id}" class="btn text-white bg-secondary btn-block editButton">
							View Enrollees
						</a>

						<a href="./editCourse.html?courseId=${course._id}" value="${course._id}" class="btn text-white bg-edit btn-block editButton">
							Edit
						</a>



						<a href="./deleteCourse.html?courseId=${course._id}&isActive=${course.isActive}" value="${course._id}" value="${course.isActive}" id="deleteBtn" class="btn ${deleteBtnColor} text-white btn-block deleteButton">
							${deleteBtn}
						</a>
					`
			}

			return (
				`
				<div class="col-md-6 my-3">
					<div class="card">
						<div class="card-body">
							<h5 class=card-title>
								${course.name}
							</h5>
							<p class="card-text text-left">
								${course.description}
							</p>
							<p class="card-text text-right">
								₱ ${course.price}
							</p>
						</div>
						<div class="card-footer">
							${cardFooter}
						</div>
					</div>
				</div>

				`
				)
		// Replaces the comma's in the array with an empty string
		}).join("");
	}

	document.querySelector("#coursesContainer").innerHTML = courseData;

})