// "window.location.search" returns the query strinng part of the URL
// console.log(window.location.search);

// Instantiate a URLSearchParams object so we can execute methods to access specific parts of the query string
let params = new URLSearchParams(window.location.search);

// The "has" method checks if the courseId key exists in the URL query string
// The method returns true if the key exists
// console.log(params.has('courseId'));

// The "get" method returns the value of the key passed in as an argument
// console.log(params.get('courseId'));

let courseId = params.get('courseId');

let token = localStorage.getItem('token');
let userId = localStorage.getItem('id');
let isAdmin = localStorage.getItem('isAdmin');

let courseName = document.querySelector('#courseName');
let courseDesc = document.querySelector('#courseDesc');
let coursePrice = document.querySelector('#coursePrice');
let enrollContainer = document.querySelector("#enrollContainer");
let usersEnrolledContainer = document.querySelector("#usersEnrolledContainer");
let enrolledUsersDiv = document.querySelector("#enrolledUsersDiv");

fetch(`${apiUrl}/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {

	// console.log(data);

	courseName.innerHTML = data.name;
	courseDesc.innerHTML = data.description;
	coursePrice.innerHTML = data.price;

	console.log(isAdmin);

	if(!isAdmin || isAdmin == "false") {

		let findUser = {userId: userId};
		let foundUser = data.enrollees.findIndex(user => {
	      if (user.userId === findUser.userId) {
	      	return true;
	      } 
	    })


		enrolledUsersDiv.innerHTML = 
		`


		`

		enrollContainer.innerHTML = 
		`	
			<button id="enrollButton" class="btn btn-block bg-green">
				Enroll
			</button>

		`

		document.querySelector("#enrollButton").addEventListener("click", () => {

			console.log(foundUser)
			if(foundUser >= 0){

				alert("You're enrolled in this course")
			}else {

				fetch(`${apiUrl}/api/users/enroll`, {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
			"Authorization": `Bearer ${token}`
		},
		body: JSON.stringify({
			courseId: courseId
		})
	})
	.then(res => res.json())
	.then(data => {

		console.log(data);

		if(data === true) {

			alert("Thank you for enrolling! See you in class!");
			window.location.replace("./courses.html");

		}else {

			alert("You must log in or register to enroll");
			window.location.replace("./register.html");
		}

		

	}) //.then /enroll endpoint

			}

	
})


	}else {

		enrollContainer.innerHTML = 
			
			`
				<div class="card">
				  <div class="card-body">
				  	<h5 class="text-center"> 
				  	</h5> 
				    <table class="table table-bordered">
				    	<thead>
						    <tr>
						      <th scope="col">Name</th>
						      <th scope="col">Enrolled On</th>						     
						    </tr>
						</thead>
					  	<tbody id='users'>								  
					  	</tbody>
					</table>
				  </div>
				</div>

			`

		

		usersEnrolledContainer.innerHTML = 

			`

			`

		let users = document.querySelector('#users');
		let fetching = document.querySelector('#fetching');
		console.log(data.enrollees);

		usersData = data.enrollees.forEach(userData => { 

			console.log(userData.userId);

			fetch(`${apiUrl}/api/users/${userData.userId}`, {
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${token}`
				}								
			})
			.then(res => res.json())
			.then(data => {
				
				console.log(data);


				fetching.innerHTML = ``
				users.innerHTML += 

				`
					<tr>
						<td>${data.firstName} ${data.lastName}</td>
						<td>${userData.enrolledOn}</td>
					</tr>
				`

			})

		})



	}

	
	

}) // .then /courses/id


