// Instantiate a URLSearchParams object so we can execute methods to access specific parts of the query string
let params = new URLSearchParams(window.location.search);

// The "get" method returns the value of the key passed in as an argument
let courseId = params.get('courseId');
let isActive = params.get('isActive')

console.log(isActive)
if (isActive === "true") {
	isActive = false;
}else {
	isActive = true;
}
console.log(isActive)
let token = localStorage.getItem('token');

fetch(`${apiUrl}/api/courses/${courseId}`, {
    method: 'DELETE',
    headers: {
    	'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
    },
    body: JSON.stringify({
    	isActive: isActive,
    	courseId: courseId
    })    
})
.then(res => res.json())
.then(data => {

	//console.log(data);

	if(data === true){

		// Delete course successful
	    // Redirect to courses page
	    alert("Course disabled/enabled successfully")
	    window.location.replace("./courses.html");



	} else {

	    // Error in deleting a course
	    alert("something went wrong");

	}

})