let token = localStorage.getItem('token');
let profileContainer = document.querySelector('#profileContainer');

if (!token || token === null){

	alert("You must login first");
	window.location.replace("./login.html");

}else {

	fetch(`${apiUrl}/api/users/details`, {
		headers: {
			"Authorization": `Bearer ${token}`
		}
	})
	.then(res => res.json())
	.then(data => {

		console.log(data);

		profileContainer.innerHTML = 

		`
		<div class="col-md-12">
					<section class="jumbotron my-2">
						<h4 class="text-center">${data.firstName} ${data.lastName}</h4>
						<h4 class="text-center"> ${data.email}</h4>
						<h4 class="text-center">${data.mobileNo}</h4>
						<h5 class="text-center mt-4" id="classhistory">Class History</h4>
						<table class="table">
							<thead>
								<tr class="labels">
									<th>Course Name</th>
									<th>Enrolled On</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody id='courses'>
							
							</tbody>
						</table>
						<a href="./courses.html" class="btn btn-outline-warning text-secondary"> Learn More Languages </a>
					</section>
				</div>
		`
		let courses = document.querySelector('#courses');

		let enrollmentData = data.enrollments.map(courseData => {

			fetch(`${apiUrl}/api/courses/${courseData.courseId}`)
			.then(res => res.json())
			.then(data => {

				console.log(data.name);

				courses.innerHTML += 

				`
					<tr>
						<td>${data.name}</td>
						<td>${courseData.enrolledOn}</td>
						<td>${courseData.status}</td>
					</tr>
					
				`
			})

			
		})

		
	})
}